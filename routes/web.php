<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BackendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/obtenerArtistas', [BackendController::class, 'obtenerArtistas']);

Route::post('/artista', [BackendController::class, 'guardarArtista']);

Route::get('/artista/{id}', [BackendController::class, 'consultarArtista']);

Route::delete('/artista/{id}', [BackendController::class, 'eliminarArtista']);

Route::patch('/artista/{id}', [BackendController::class, 'actualizarArtista']);

